import './App.css';
import RouterComponent from './route/Routes';

function App() {
  return (
      <>
        <RouterComponent/>
      </>
  );
}

export default App;
