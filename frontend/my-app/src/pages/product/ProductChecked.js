import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "../../Axios";
import { EditOutlined, DeleteOutlined, InfoCircleOutlined, UserOutlined, CheckCircleOutlined } from '@ant-design/icons';
import { Button, Card, Form, Input, message, Modal, Table, Tooltip } from "antd";


const ProductListChecked = () => {
    const [productList, setProductList] = useState([])
    const [userDetail, setUserDetail] = useState([])
    const [fetchStatus, setFetchStatus] = useState(true);
    const [description, setDescription] = useState(0)
    const [name, setName] = useState("")

    let history = useHistory()
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [isModalVisibleSigned, setIsModalVisibleSigned] = useState(false);
    const [form] = Form.useForm();

    const handleCancel = () => {
        setIsModalVisible(false);
        setIsModalVisibleSigned(false);
    };

    useEffect(() => {
        const fetchData = async () => {
            try {
                const {data} = await axios.get(`/productsChecked`, {
                    headers: {"Authorization" : localStorage.getItem('token')}
                })
                let result = data.data.map((res) => {
                    let {id, name, description, status} = res
                    // let roleID = res.role.id
                    // let roleTitle = res.role.title
                    return {
                        id,
                        name,
                        description,
                        status
                    }
                })
                console.log(result)
                setProductList([...result])
            } catch (error) {
                alert('file failed to show')
                console.log(error)
            }
        }
        if (fetchStatus) {
            fetchData()
            setFetchStatus(false)
          }

    }, [fetchStatus, setFetchStatus])

    const handleEdit = (event) => {
        let id = event.currentTarget.value
        localStorage.setItem("idProduct", id)
        axios.get(`product/${id}`, {
            headers: {"Authorization" : localStorage.getItem('token')}
        })
        .then(res => {
            let data = res.data.data;
            console.log(data)
            form.setFieldsValue({
                name: data.name,
                description: data.description
            })
        })
        setIsModalVisible(true);
    }

    const handleUpdateProduct = (event) => {
        event.preventDefault()
        // handleChange(event)
        var idProduct = localStorage.getItem("idProduct")
        console.log(idProduct)
        axios
        .put(`/product/${idProduct}`, {
            name: name,
            description: description
        }, {
            headers: {"Authorization" : localStorage.getItem('token')}
        })
        .then((res) => {
            message.success('Update Product Success');
            console.log(res.data);
            setIsModalVisible(false);
        })
        .catch((error) => {
            console.log(error)
            // message.warning(error);
        })
    }
  
    const handleEditSigned = (event) => {
        let id = event.currentTarget.value
        localStorage.setItem("idProduct", id)
        axios.get(`product/${id}`, {
            headers: {"Authorization" : localStorage.getItem('token')}
        })
        .then(res => {
            let data = res.data.data;
            console.log(data)
            form.setFieldsValue({
                name: data.name,
                description: data.description
            })
            setName(data.name)
            setDescription(data.description)
        })
        setIsModalVisibleSigned(true);
    }

    const handleSignedProduct = (event) => {
        event.preventDefault()
        var idProduct = localStorage.getItem("idProduct")
        console.log(idProduct)
        axios
        .put(`/product/${idProduct}/signer`, {
            name: name,
            description: description
        }, {
            headers: {"Authorization" : localStorage.getItem('token')}
        })
        .then((res) => {
            message.success('Signed Product Success');
            console.log(res.data);
            setIsModalVisibleSigned(false);
        })
        .catch((error) => {
            console.log(error.response.data.status)
            message.warning(error.response.data.status);
        })
    }

    const handleDetail = (event) => {
        let id = event.currentTarget.value
        localStorage.setItem("idProduct", id)
        history.push(`/productdetail/${id}`)
    }

    const handleDelete = (event) => {
        let id = event.currentTarget.value
        if (window.confirm("Are you sure to delete this task?")) {
            axios.delete(`product/${id}`, {
                headers: {"Authorization" : localStorage.getItem('token')}
            })
            .then(() => {
                setFetchStatus(true)
            })
            .catch((error) => {
                console.log(error.response.data.status)
                message.warning(error.response.data.status);
            })
        }
    }

    const blue = 'blue'
    const cyan = 'cyan'
    const cyan1 = 'cyan'
    const red = 'red'
    const columns = [
        {
          title: 'ID',
          dataIndex: 'id',
          key: 'id',
          
        },
        {
          title: 'Name',
          dataIndex: 'name',
          key: 'name',
          
        },
        {
          title: 'Description',
          dataIndex: 'description',
          key: 'description',
        },
        {
          title: 'Status',
          dataIndex: 'status',
          key: 'status',
        },
        {
          title: 'Action',
        //   dataIndex: 'id',
          key: 'action',
          render: (res, index) => (
            <div>
                {/* {console.log(res.id)} */}
                <li key={index}></li>
                <Tooltip title="View" color={blue} key={blue}>
                    <button onClick={handleDetail} value={res.id}><InfoCircleOutlined /></button>
                </Tooltip>
                <Tooltip title="Edit" color={cyan} key={cyan}>
                    <button onClick={handleEdit} value={res.id}><EditOutlined /></button>
                </Tooltip>
                <Tooltip title="Signed" color={cyan1} key={cyan1}>
                    <button onClick={handleEditSigned} value={res.id}><CheckCircleOutlined /></button>
                </Tooltip>
                <Tooltip title="Delete" color={red} key={red}>
                    <button onClick={handleDelete} value={res.id}><DeleteOutlined /></button>
                </Tooltip>
            </div>
        ),

        },
      ];
      
    return (
        <>
            <Table
                columns={columns}
                dataSource={productList}
                bordered
                size="middle"
                scroll={{ x: 'calc(700px + 50%)', y: 400 }}
            />

            <Modal title="Basic Modal" visible={isModalVisible} onOk={handleUpdateProduct} onCancel={handleCancel} footer={[
                <Button type="primary" htmlType="submit" className="login-form-button" onClick={handleUpdateProduct}>
                Update
                </Button>
            ]}>
            <Card title="UPDATE NEW PRODUCT" style={{ width: 400 }}>
                <Form
                    name="normal_login"
                    className="login-form"
                    // onSubmit={handleSubmitProduct}
                    method="post"
                    form={form}
                    >
                    <Form.Item
                        name="name"
                        rules={[
                        {
                            required: true,
                            message: 'Please input product name!',
                        },
                        ]}
                    >
                        <Input 
                        prefix={<UserOutlined className="site-form-item-icon" />} 
                        placeholder="product name" 
                        type="text"
                        onChange={(event) => setName(event.target.value)}
                        value={name}
                        />
                    </Form.Item>
                    <Form.Item
                        name="description"
                        rules={[
                        {
                            required: true,
                            message: 'Please input product description!',
                        },
                        ]}
                    >
                        <Input 
                        prefix={<UserOutlined className="site-form-item-icon" />} 
                        placeholder="decription" 
                        type="text"
                        onChange={(event) => setDescription(event.target.value)}
                        value={description}
                        />
                    </Form.Item>

                    {/* <Form.Item>
                        <Button type="primary" htmlType="submit" className="login-form-button" onClick={handleUpdateProduct}>
                        Insert
                        </Button>
                    </Form.Item> */}
                </Form>
            </Card>
            </Modal>

            <Modal title="Basic Modal" visible={isModalVisibleSigned} onOk={handleSignedProduct} onCancel={handleCancel} footer={[
                <Button type="primary" htmlType="submit" className="login-form-button" onClick={handleSignedProduct}>
                Signed
                </Button>
            ]}>
            <Card title="SIGNED NEW PRODUCT" style={{ width: 400 }}>
                <Form
                    name="normal_login"
                    className="login-form"
                    // onSubmit={handleSubmitProduct}
                    method="post"
                    form={form}
                    >
                    <Form.Item
                        name="name"
                        rules={[
                        {
                            required: true,
                            message: 'Please input product name!',
                        },
                        ]}
                    >
                        <Input 
                        prefix={<UserOutlined className="site-form-item-icon" />} 
                        placeholder="product name" 
                        type="text"
                        onChange={(event) => setName(event.target.value)}
                        value={name}
                        disabled
                        />
                    </Form.Item>
                    <Form.Item
                        name="description"
                        rules={[
                        {
                            required: true,
                            message: 'Please input product description!',
                        },
                        ]}
                    >
                        <Input 
                        prefix={<UserOutlined className="site-form-item-icon" />} 
                        placeholder="decription" 
                        type="text"
                        onChange={(event) => setDescription(event.target.value)}
                        value={description}
                        disabled
                        />
                    </Form.Item>

                    {/* <Form.Item>
                        <Button type="primary" htmlType="submit" className="login-form-button" onClick={handleUpdateProduct}>
                        Insert
                        </Button>
                    </Form.Item> */}
                </Form>
            </Card>
            </Modal>            

        </>
    )
}
export default ProductListChecked;