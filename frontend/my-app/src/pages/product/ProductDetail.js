import { Divider, List, Typography } from "antd";
import axios from "../../Axios";
import React, { useEffect, useState } from "react";

const ProductDetail = () => {

    const [productDetail, setProductDetail] = useState({}
        // id : 0,
        // name : "",
        // email : ""
    )
    const [fetchStatus, setFetchStatus] = useState(true)
    var idProduct = localStorage.getItem("idProduct")

    useEffect(() => {
        const fetchData = async () => {
            try {
                const {data} = await axios.get(`/product/${idProduct}`, {
                    headers: {"Authorization" : localStorage.getItem('token')}
                })
                // .then(res => {
                let result = data.data
                // let roleID = result.role.id
                console.log(result)
                setProductDetail({
                    id : result.id,
                    name : result.name,
                    status : result.status,
                    description : result.description,
                    checkerID : result.checker.id,
                    checkerName: result.checker.name,
                    makerID : result.maker.id,
                    makerName : result.maker.name,
                    signerID : result.signer.id,
                    signerName : result.signer.name
                    })
                }
                catch (error) {
                alert('file failed to show')
                console.log(error)
                }
        }
        if (fetchStatus) {
            fetchData()
            setFetchStatus(false)
        }
    }, [fetchStatus, setFetchStatus])
    // console.log(userDetail)
    const data = [
        // console.log(productDetail.id),
        'ID                 : ' + productDetail.id,
        'Name               : ' + productDetail.name,
        'Description        : ' + productDetail.description,
        'Status             : ' + productDetail.status,
        'Checker ID         : ' + productDetail.checkerID,
        'Checker Name       : ' + productDetail.checkerName,
        'Maker ID           : ' + productDetail.makerID,
        'Maker Name         : ' + productDetail.makerName,
        'Signer ID          : ' + productDetail.signerID,
        'Signer Name        : ' + productDetail.signerName
    ];
    return (
        <>
            <Divider orientation="left">User Detail</Divider>
                <List
                // header={<div>Header</div>}
                // footer={<div>Footer</div>}
                bordered
                // dataSource={console.log(data)}
                dataSource={data}
                // key={index}
                renderItem={item => (
                    <List.Item>
                     {item}
                    </List.Item>
                )}
            />
        </>
    )
}
export default ProductDetail;