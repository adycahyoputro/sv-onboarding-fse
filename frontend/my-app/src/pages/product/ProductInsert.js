import { Button, Card, Checkbox, Form, Input, message } from "antd";
import React, { useState } from "react";
import 'antd/dist/antd.css';
import '../../assets/Style.css';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import axios from "../../Axios";
import { useHistory } from "react-router-dom";

const ProductInsert = () => {
    const history = useHistory();
    const [description, setDescription] = useState(0)
    const [name, setName] = useState('')

    const handleSubmitProduct = (event) => {
        event.preventDefault()
        axios
        .post(`/product`, {
            name : name,
            description : description
        }, {
            headers: {"Authorization" : localStorage.getItem('token')}
        })
        .then((res) => {
            history.push('/productlist');
            message.success('Insert New Product Success');
            console.log(res.data);
        })
        .catch((error) => {
            console.log(error.response.data.status)
            message.warning(error.response.data.status);
        })
    }
    return (
        <>
            <div className="cardFormLogin">
            <Card title="INSERT NEW PRODUCT" style={{ width: 400 }}>
                <Form
                    name="normal_login"
                    className="login-form"
                    onSubmit={handleSubmitProduct}
                    method="post"
                    >
                    <Form.Item
                        name="username"
                        rules={[
                        {
                            required: true,
                            message: 'Please input product name!',
                        },
                        ]}
                    >
                        <Input 
                        prefix={<UserOutlined className="site-form-item-icon" />} 
                        placeholder="product name" 
                        type="text"
                        onChange={(event) => setName(event.target.value)}
                        value={name}
                        />
                    </Form.Item>
                    <Form.Item
                        name="decription"
                        rules={[
                        {
                            required: true,
                            message: 'Please input product description!',
                        },
                        ]}
                    >
                        <Input 
                        prefix={<UserOutlined className="site-form-item-icon" />} 
                        placeholder="decription" 
                        type="text"
                        onChange={(event) => setDescription(event.target.value)}
                        value={description}
                        />
                    </Form.Item>

                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="login-form-button" onClick={handleSubmitProduct}>
                        Insert
                        </Button>
                    </Form.Item>
                </Form>
            </Card>
            </div>
        </>
    )
}
export default ProductInsert;