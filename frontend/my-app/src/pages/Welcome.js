import React, { useEffect, useState } from "react";
import axios from "../Axios";

const Welcome = () => {
    const [userDetail, setUserDetail] = useState({

    })
    // const [fetchStatus, setFetchStatus] = useState(true);

    const request =  () => {
        let id = localStorage.getItem('id');
        axios.get(`/user/${id}`, {
            headers: {"Authorization" : localStorage.getItem('token')}
        })
        .then(res => {    
            setUserDetail(res.data.data)
        })
        .catch((error) => {
            alert('file failed to show')
            console.log(error)
        })
    }
   

    useEffect(() =>{
        request();
    }, [])


    

    return (
        <>
           <p>Selamat Datang {userDetail.name}</p>
        </>
    )
}

export default Welcome;