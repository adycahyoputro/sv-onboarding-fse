import { Button, Card, Form, Input, message, Modal, Radio, Select, Space, Table, Tag, Tooltip } from "antd";
import React, { useEffect, useState } from "react";
import axios from "../../Axios";
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { useHistory } from "react-router-dom";
import { UserOutlined, InfoCircleOutlined, LockOutlined } from '@ant-design/icons';

const UserList = () => {
    const [userList, setUserList] = useState([])
    const [roleList, setRoleList] = useState([])
    const [userDetail, setUserDetail] = useState([])
    const [fetchStatus, setFetchStatus] = useState(true);
    const [fetchStatusRoles, setFetchStatusRoles] = useState(true);
    const [input, setInput] = useState({
        name : "",
        personalNumber : 0,
        email : "",
        roleID : "",
        active : ""
    })
    const [value, setValue] = useState([]);
    const { Option } = Select;
    const [personalNumber, setPersonalNumber] = useState(0)
    const [roleId, setRoleId] = useState('')
    const [email, setEmail] = useState('')
    const [name, setName] = useState('')
    const [active, setActive] = useState(Boolean)
    

    let history = useHistory()

    const [isModalVisible, setIsModalVisible] = useState(false);

    // const showModal = () => {
    //     setIsModalVisible(true);
    // };

    // const handleOk = () => {
    //     setIsModalVisible(false);
    // };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    useEffect(() => {
        const fetchData = async () => {
            try {
                const {data} = await axios.get(`/users`, {
                    headers: {"Authorization" : localStorage.getItem('token')}
                })
                let result = data.data.map((res) => {
                    let {id, name, role, active} = res
                    let roleID = res.role.id
                    let roleTitle = res.role.title
                    return {
                        id,
                        name,
                        role,
                        roleID,
                        roleTitle,
                        active
                    }
                })
                console.log(result)
                setUserList([...result])
            } catch (error) {
                alert('file failed to show')
                console.log(error)
            }
        }
        if (fetchStatus) {
            fetchData()
            setFetchStatus(false)
          }

    }, [fetchStatus, setFetchStatus])
  
    const handleDetail = (event) => {
        let id = event.currentTarget.value
        localStorage.setItem("idUser", id)
        history.push(`/userdetail/${id}`)
    }

    const handleChange = (event) => {
        let {name, value} = event.target;
        setInput({...input, [name] : value});
    }

    // handleRoles = () => {
        useEffect(() => {
            const fetchDataRoles = async () => {
                try {
                    const {data} = await axios.get(`/roles`, {
                        headers: {"Authorization" : localStorage.getItem('token')}
                    })
                    let result = data.data.map((resRoles) => {
                        let {id, title} = resRoles
                        return {
                            id,
                            title
                        }
                    })
                    console.log(result)
                    setRoleList([...result])
                } catch (error) {
                    alert('file failed to show')
                    console.log(error)
                }
            }
            if (fetchStatusRoles) {
                fetchDataRoles()
                setFetchStatusRoles(false)
              }
    
        }, [fetchStatusRoles, setFetchStatusRoles])
    // }

    const handleEdit = (event) => {
        let id = event.currentTarget.value
        localStorage.setItem("idUser", id)
        axios.get(`user/${id}`, {
            headers: {"Authorization" : localStorage.getItem('token')}
        })
        .then(res => {
            let data = res.data.data;
            console.log(data)
            // setName(data.name)
            // setEmail(data.email)
            // setPersonalNumber(data.personalNumber)
            form.setFieldsValue({
                name: data.name,
                personalNumber: data.personalNumber,
                email: data.email,
                roleId: data.role.id,
                active: data.active
            })
            // setInput({
            //     name : data.name,
            //     email : data.email,
            //     personalNumber : data.personalNumber
            // })
            // history.push(`/useredit/${id}`)
        })
        setIsModalVisible(true);
    }

    const handleUpdate = (event) => {
        event.preventDefault()
        var idUser = localStorage.getItem("idUser")
        // console.log(idUser)
        axios
        .put(`/user/${idUser}`, {
            name :name ,
            personalNumber :personalNumber ,
            email :email ,
            active :active,
            role : {
                id:roleId
            },
            password : ""
        }, {
            headers: {"Authorization" : localStorage.getItem('token')}
        })
        .then((res) => {
            message.success('Update User Success');
            console.log(res.data);
            setIsModalVisible(false);
        })
        .catch((error) => {
            console.log(error.response.data.status)
            message.warning(error.response.data.status);
        })
        // setInput({
        //     name : "",
        //     email : "",
        //     personalNumber : 0,
        //     roleID : ""
        // })
    }
    const handleDelete = (event) => {
        let id = event.currentTarget.value
        if (window.confirm("Are you sure to delete this task?")) {
            axios.delete(`user/${id}`, {
                headers: {"Authorization" : localStorage.getItem('token')}
            })
            .then(() => {
                setFetchStatus(true)
            })
            .catch((error) => {
                console.log(error.response.data.status)
                message.warning(error.response.data.status);
            })
        }
    }

    const blue = 'blue'
    const cyan = 'cyan'
    const red = 'red'
    const columns = [
        {
          title: 'ID',
          dataIndex: 'id',
          key: 'id',
          
        },
        {
          title: 'Name',
          dataIndex: 'name',
          key: 'name',
          
        },
        {
            title: 'Role',
            children: [
            {
                title: 'ID',
                dataIndex: 'roleID',
                key: 'roleID',
            },
            {
                title: 'Title',
                dataIndex: 'roleTitle',
                key: 'roleTitle',
            },
            ],
        },
        {
          title: 'Status',
          dataIndex: 'active',
          key: 'active',
          render: (active, index) => (
              <p>
                  {active == true ? "Active" : "Inactive"}
                  {/* {console.log(active)} */}
              </p>    
          )
        },
        {
          title: 'Action',
        //   dataIndex: 'id',
          key: 'action',
          render: (res, index) => (
            <div>
                {/* {console.log(res.id)} */}
                <li key={index}></li>
                <Tooltip title="View" color={blue} key={blue}>
                    <button onClick={handleDetail} value={res.id}><InfoCircleOutlined /></button>
                </Tooltip>
                <Tooltip title="Edit" color={cyan} key={cyan}>
                    <button onClick={handleEdit} value={res.id}><EditOutlined /></button>
                </Tooltip>
                <Tooltip title="Delete" color={red} key={red}>
                    <button onClick={handleDelete} value={res.id}><DeleteOutlined /></button>
                </Tooltip>
            </div>
        ),

        },
      ];
      const [form] = Form.useForm();
      function handleChangeSelect(value) {
        setRoleId(value);
        console.log(`selected ${value}`);
      }
      
    return (
        <>
            <Table
                columns={columns}
                dataSource={userList}
                bordered
                size="middle"
                scroll={{ x: 'calc(700px + 50%)', y: 400 }}
            />

            <Modal title="Basic Modal" visible={isModalVisible} onOk={handleUpdate} onCancel={handleCancel}>
            <Card title="DATA USER" style={{ width: 400 }}>
                <Form
                    name="normal_login"
                    className="login-form"
                    onSubmit={handleUpdate}
                    method="post"
                    form={form}
                    >
                    <Form.Item
                        name="name"
                        rules={[
                        {
                            required: true,
                            message: 'Please input your Username!',
                        },
                        ]}
                    >
                        <Input 
                        prefix={<UserOutlined className="site-form-item-icon" />} 
                        placeholder="Username" 
                        type="text"
                        onChange={(event) => setName(event.target.value)}
                        value={name}
                        // value={console.log(input.name)}
                        // initialValue={input.name}
                        />
                    </Form.Item>
                    <Form.Item
                        name="personalNumber"
                        rules={[
                        {
                            required: true,
                            message: 'Please input your Personal Number!',
                        },
                        ]}
                    >
                        <Input 
                        prefix={<UserOutlined className="site-form-item-icon" />} 
                        placeholder="Personal Number" 
                        type="number"
                        onChange={(event) => setPersonalNumber(event.target.value)}
                        value={personalNumber}
                        />
                    </Form.Item>
                    <Form.Item
                        name="email"
                        rules={[
                        {
                            required: true,
                            message: 'Please input your Email!',
                        },
                        ]}
                    >
                        <Input 
                        prefix={<UserOutlined className="site-form-item-icon" />} 
                        placeholder="Email" 
                        type="email"
                        onChange={(event) => setEmail(event.target.value)}
                        value={email}
                        />
                    </Form.Item>
                    {/* <Form.Item
                        name="roleID"
                        rules={[
                        {
                            required: true,
                            message: 'Please input your role id!',
                        },
                        ]}
                    >
                        <Input
                        prefix={<UserOutlined className="site-form-item-icon" />}
                        type="text"
                        placeholder="role id"
                        onChange={(event) => setRoleId(event.target.value)}
                        value={roleId}
                        disabled
                        />
                    </Form.Item> */}
                    <Form.Item
                        name="roleId"
                        rules={[
                        {
                            required: true,
                            message: 'Please input your role id!',
                        },
                        ]}
                    >
                        <Select name={roleId} style={{ width: 120 }} onChange={handleChangeSelect}>                                        
                        {roleList.map((val, index) => 
                        // {
                        //     return 
                            (
                                <>
                                {/* {console.log(val)} */}
                                        <Option value={val.id} key={index}>{val.title}</Option>
                                </>
                            )
                        // }
                        )}
                        </Select>
                        {/* {handleRole} */}
                    </Form.Item>
                    <Form.Item
                        name="active"
                        rules={[
                        {
                            required: true,
                            message: 'Please select status!',
                        },
                        ]}
                    >
                        <Radio.Group onClick={(event) => alert(event.currentTarget.value)}>
                            <Space direction="vertical">
                                <Radio value={true} onClick={(event) => setActive(true)}>Active</Radio>
                                <Radio value={false} onClick={(event) => setActive(false)}>Inactive</Radio>
                            </Space>
                        </Radio.Group>
                    </Form.Item>

                    {/* <Form.Item>
                        <Button type="primary" htmlType="submit" className="login-form-button" onClick={handleUpdate}>
                        Update
                        </Button>
                    </Form.Item> */}
                </Form>
            </Card>
            </Modal>

        </>
    )
}
export default UserList