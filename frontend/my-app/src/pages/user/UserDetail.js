import { Divider, List, Typography } from "antd";
import axios from "../../Axios";
import React, { useEffect, useState } from "react";

const UserDetail = () => {

    const [userDetail, setUserDetail] = useState({}
        // id : 0,
        // name : "",
        // email : ""
    )
    const [fetchStatus, setFetchStatus] = useState(true)
    var idUser = localStorage.getItem("idUser")

    useEffect(() => {
        const fetchData = async () => {
            try {
                const {data} = await axios.get(`/user/${idUser}`, {
                    headers: {"Authorization" : localStorage.getItem('token')}
                })
                // .then(res => {
                let result = data.data
                // let roleID = result.role.id
                console.log(result)
                setUserDetail({
                    id : result.id,
                    name : result.name,
                    email : result.email,
                    personalNumber : result.personalNumber,
                    roleID : result.role.id,
                    title : result.role.title
                    })
                }
                catch (error) {
                alert('file failed to show')
                console.log(error)
                }
        }
        if (fetchStatus) {
            fetchData()
            setFetchStatus(false)
        }
    }, [fetchStatus, setFetchStatus])
    // console.log(userDetail)
    const data = [
        // console.log(userDetail.id)
        'ID      : ' + userDetail.id,
        'Name               :' + userDetail.name,
        'Email              :' + userDetail.email,
        'Personal Number    : ' + userDetail.personalNumber,
        'Role ID            : ' + userDetail.roleID,
        'Title              : ' + userDetail.title
    ];
    return (
        <>
            <Divider orientation="left">User Detail</Divider>
                <List
                // header={<div>Header</div>}
                // footer={<div>Footer</div>}
                bordered
                // dataSource={console.log(data)}
                dataSource={data}
                // key={index}
                renderItem={item => (
                    <List.Item>
                     {item}
                    </List.Item>
                )}
            />
        </>
    )
}
export default UserDetail;