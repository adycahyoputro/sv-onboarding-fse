import { Button, Card, Checkbox, Form, Input, message } from "antd";
import React, { useState } from "react";
import 'antd/dist/antd.css';
import '../../assets/Style.css';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import axios from "../../Axios";
import { useHistory } from "react-router-dom";

const UserInsert = () => {
    const history = useHistory();
    const [personalNumber, setPersonalNumber] = useState(0)
    const [password, setPassword] = useState('')
    const [email, setEmail] = useState('')
    const [name, setName] = useState('')

    const handleSubmitRegister = (event) => {
        event.preventDefault()
        axios
        .post(`/user`, {
            name : name,
            personalNumber : personalNumber,
            email : email,
            password : password
        })
        .then((res) => {
            history.push('/userlist');
            message.success('Insert New User Success');
            console.log(res.data);
        })
        .catch((error) => {
            console.log(error.response.data.status)
            message.warning(error.response.data.status);
        })
    }
    return (
        <>
            <div className="cardFormLogin">
            <Card title="INSERT NEW USER" style={{ width: 400 }}>
                <Form
                    name="normal_login"
                    className="login-form"
                    onSubmit={handleSubmitRegister}
                    method="post"
                    >
                    <Form.Item
                        name="username"
                        rules={[
                        {
                            required: true,
                            message: 'Please input your Username!',
                        },
                        ]}
                    >
                        <Input 
                        prefix={<UserOutlined className="site-form-item-icon" />} 
                        placeholder="Username" 
                        type="text"
                        onChange={(event) => setName(event.target.value)}
                        value={name}
                        />
                    </Form.Item>
                    <Form.Item
                        name="personalNumber"
                        rules={[
                        {
                            required: true,
                            message: 'Please input your Personal Number!',
                        },
                        ]}
                    >
                        <Input 
                        prefix={<UserOutlined className="site-form-item-icon" />} 
                        placeholder="Personal Number" 
                        type="number"
                        onChange={(event) => setPersonalNumber(event.target.value)}
                        value={personalNumber}
                        />
                    </Form.Item>
                    <Form.Item
                        name="email"
                        rules={[
                        {
                            required: true,
                            message: 'Please input your Email!',
                        },
                        ]}
                    >
                        <Input 
                        prefix={<UserOutlined className="site-form-item-icon" />} 
                        placeholder="Email" 
                        type="email"
                        onChange={(event) => setEmail(event.target.value)}
                        value={email}
                        />
                    </Form.Item>
                    <Form.Item
                        name="password"
                        rules={[
                        {
                            required: true,
                            message: 'Please input your Password!',
                        },
                        ]}
                    >
                        <Input
                        prefix={<LockOutlined className="site-form-item-icon" />}
                        type="password"
                        placeholder="Password"
                        onChange={(event) => setPassword(event.target.value)}
                        value={password}
                        />
                    </Form.Item>

                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="login-form-button" onClick={handleSubmitRegister}>
                        Insert
                        </Button>
                    </Form.Item>
                </Form>
            </Card>
            </div>
        </>
    )
}
export default UserInsert;