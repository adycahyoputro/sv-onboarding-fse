import { Button, Card, Form, Input, message } from "antd";
import React, { useState } from "react";
import 'antd/dist/antd.css';
import '../../assets/Style.css';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import axios from "../../Axios";
import { useHistory } from "react-router-dom";
// import Cookies from "js-cookie";

const Login = () => {
    // const onFinish = (values) => {
    //     console.log('Received values of form: ', values);
    //   };
    // const [input, setInput] = useState({
    //     personalNumber : 0,
    //     password : ""
    // })
    const history = useHistory();
    const [personalNumber, setPersonalNumber] = useState(0)
    const [password, setPassword] = useState('')

    const handleSubmitLogin = (event) => {
        event.preventDefault()
        axios
        .post(`/login`, {
            personalNumber : personalNumber,
            password : password
        })
        .then((res) => {
            var token = res.data.data.token;
            var id = res.data.data.id;
            // Cookies.set('token', token);
            localStorage.setItem('token', token);
            localStorage.setItem('id', id);
            history.push('/');
            message.success('Login success');
            // console.log(res.data);
        })
        .catch((error) => {
            console.log(error.response.data.error.message)
            message.warning(error.response.data.error.message);
            // <Alert message="error.response.data.error.message" type="warning" showIcon closable />
        })
    }
    return (
        <>
            <div className="cardFormLogin">
            <Card title="LOGIN" style={{ width: 400 }}>
                <div className="formLogin">
                <Form
                    name="normal_login"
                    className="login-form"
                    // initialValues={{
                    //     remember: true,
                    // }}
                    // onFinish={handleSubmitLogin}
                    // onSubmit={handleSubmitLogin}
                    >
                    <Form.Item
                        name="personalNumber"
                        rules={[
                        {
                            required: true,
                            message: 'Please input your Personal Number!',
                        },
                        ]}
                    >
                        <Input 
                        prefix={<UserOutlined className="site-form-item-icon" />} 
                        placeholder="Personal Number" 
                        type="number"
                        onChange={(event) => setPersonalNumber(event.target.value)}
                        value={personalNumber}
                        />
                    </Form.Item>
                    <Form.Item
                        name="password"
                        rules={[
                        {
                            required: true,
                            message: 'Please input your Password!',
                        },
                        ]}
                    >
                        <Input
                        prefix={<LockOutlined className="site-form-item-icon" />}
                        type="password"
                        placeholder="Password"
                        onChange={(event) => setPassword(event.target.value)}
                        value={password}
                        />
                    </Form.Item>

                    <Form.Item className="buttonLogin">
                        <Button type="primary" htmlType="submit" className="login-form-button"
                        onClick={handleSubmitLogin}
                        >
                        Log in
                        </Button>
                        <br/>Or <a href="/register">register now!</a>
                    </Form.Item>
                </Form>
                </div>
            </Card>
            </div>
        </>
    )
}
export default Login;