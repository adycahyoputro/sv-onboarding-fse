import axios from "axios";

const instance = axios.create({
    //server local
    baseURL: 'http://localhost:8080'
})
export default instance