import React from "react";
import { Redirect } from "react-router-dom";
import { BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import Dashboard from "../layout/Dashboard";
import Login from "../pages/authentication/Login";
import Register from "../pages/authentication/Register";
import ProductListChecked from "../pages/product/ProductChecked";
import ProductDetail from "../pages/product/ProductDetail";
import ProductInsert from "../pages/product/ProductInsert";
import ProductList from "../pages/product/ProductList";
import ProductListSigned from "../pages/product/ProductSigned";
import UserDetail from "../pages/user/UserDetail";
import UserInsert from "../pages/user/UserInsert";
import UserList from "../pages/user/UserList";
import Welcome from "../pages/Welcome";

const RouterComponent = () => {

    const PrivateRoute = ({ ...props}) => { 
        // console.log(localStorage.getItem('token')) 
        if(localStorage.getItem('token') !== null) {
            return <Route {...props} />
        }else if(localStorage.getItem('token') === null ) {
            return <Redirect to="/login" />
        } 
      }

      const LoginRoute = ({ ...props}) => { 
        if(localStorage.getItem('token') === null) {
            return <Route {...props} />
        }else if(localStorage.getItem('token') !== null ) {
            return <Redirect to="/" />
        } 
      }
      
    return (
        <>
            <Router>
                <Switch>
                    <PrivateRoute path="/" exact>
                        <Dashboard main content={<Welcome/>}/>
                    </PrivateRoute>
                    <Route path="/login" exact>
                        <Dashboard login content={<Login />}/>    
                    </Route>
                    <LoginRoute path="/register" exact>
                        <Dashboard register content={<Register />}/>
                    </LoginRoute>
                    <PrivateRoute path="/userlist" exact>
                        <Dashboard main content={<UserList/>}/>
                    </PrivateRoute>
                    <PrivateRoute path="/userdetail/:slug" exact>
                        <Dashboard main content={<UserDetail/>}/>
                    </PrivateRoute>
                    <PrivateRoute path="/userinsert" exact>
                        <Dashboard main content={<UserInsert/>}/>
                    </PrivateRoute>
                    <PrivateRoute path="/useredit/:slug" exact>
                        <Dashboard main content={<UserInsert/>}/>
                    </PrivateRoute>
                    <PrivateRoute path="/productlist" exact>
                        <Dashboard main content={<ProductList/>}/>
                    </PrivateRoute>
                    <PrivateRoute path="/productlistchecked" exact>
                        <Dashboard main content={<ProductListChecked/>}/>
                    </PrivateRoute>
                    <PrivateRoute path="/productlistsigned" exact>
                        <Dashboard main content={<ProductListSigned/>}/>
                    </PrivateRoute>
                    <PrivateRoute path="/productdetail/:slug" exact>
                        <Dashboard main content={<ProductDetail/>}/>
                    </PrivateRoute>
                    <PrivateRoute path="/productinsert" exact>
                        <Dashboard main content={<ProductInsert/>}/>
                    </PrivateRoute>
                </Switch>
            </Router>
        </>
    )
}
export default RouterComponent;