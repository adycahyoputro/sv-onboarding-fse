import { Breadcrumb, Layout, Menu } from "antd";
import React, { useState } from "react";
import '../assets/Style.css';
import {
    FileOutlined,
    TeamOutlined,
    UserOutlined,
  } from '@ant-design/icons';
  import { Link } from "react-router-dom";
  import { useHistory } from "react-router-dom";
//   import 'antd/dist/antd.css';
const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

const Dashboard = (props) => {
    let history = useHistory()
    const [state, setState] = useState({
        collapsed: false
    })

 const onCollapse = (collapsed) => {
    console.log(collapsed);
    setState({ collapsed });
  };
  const { collapsed } = state;

  const HandleLogOut = () => {
    localStorage.removeItem('token')
    localStorage.removeItem('id')
    localStorage.removeItem('idUser')
    localStorage.removeItem('idProduct')
    history.push('/login');
  }
    return (
        <>
        {props.login && props.content}
        {props.register && props.content}
        {props.main && (
            <Layout>
                <Header className="header">
                    <div className="logo">
                        {/* <img src="../assets/ant_design_training.png" width={10}/> */}
                        {/* <Image src="../assets/ant_design_training.png" width={200} /> */}
                    </div>
                <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
                    <Menu.Item key="0"><Link to={'/'}><img src="../assets/ant_design_training.png" width={30}/></Link></Menu.Item>
                    <Menu.Item key="1"><Link to={'/'}>Home</Link></Menu.Item>
                    <Menu.Item key="2"><Link to={'/about'}>About</Link></Menu.Item>
                    <Menu.Item key="3" style={{position: "absolute", right : "0"}} onClick={HandleLogOut}><span>Log Out</span></Menu.Item>
                </Menu>
                </Header>
                <Layout style={{ minHeight: '100vh' }}>
                    <Sider collapsible collapsed={collapsed} onCollapse={onCollapse}>
                    <Menu theme="dark" mode="inline">
                        <SubMenu key="sub1" icon={<UserOutlined />} title="User">
                        <Menu.Item key="1"><Link to={"/userlist"}>User List</Link></Menu.Item>
                        <Menu.Item key="2"><Link to={"/userinsert"}>Insert New User</Link></Menu.Item>
                        </SubMenu>
                        <SubMenu key="sub2" icon={<TeamOutlined />} title="Product">
                        <Menu.Item key="3"><Link to={"/productinsert"}>Insert New Product</Link></Menu.Item>
                        <Menu.Item key="4"><Link to={"/productlist"}>Product List</Link></Menu.Item>
                        <Menu.Item key="5"><Link to={"/productlistchecked"}>Product Checked</Link></Menu.Item>
                        <Menu.Item key="6"><Link to={"/productlistsigned"}>Product Signed</Link></Menu.Item>
                        </SubMenu>
                        {/* <Menu.Item key="6" icon={<FileOutlined />}>
                        Files
                        </Menu.Item> */}
                    </Menu>
                    </Sider>
                    <Layout className="site-layout">
                    {/* <Header className="site-layout-background" style={{ padding: 0 }} /> */}
                    <Content style={{ margin: '0 16px' }}>
                        <Breadcrumb style={{ margin: '16px 0' }}>
                        <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
                        </Breadcrumb>
                        <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
                            {props.content}
                        </div>
                    </Content>
                    <Footer style={{ textAlign: 'center' }}>Ant Design ©2022 Created by Ant UED</Footer>
                    </Layout>
                </Layout>
            </Layout>
        )}
        </>
    )
}
export default Dashboard;