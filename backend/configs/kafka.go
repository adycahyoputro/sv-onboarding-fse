package configs

import (
	"log"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/riferrei/srclient"
)

func InitProducer() (*kafka.Producer, error) {
	producer, err := kafka.NewProducer(&kafka.ConfigMap{
		"bootstrap.servers": CONFIG["KAFKA_SERVERS"],
		"sasl.mechanisms":   "PLAIN",
		"security.protocol": "SASL_SSL",
		"sasl.username":     CONFIG["KAFKA_SASL_USERNAME"],
		"sasl.password":     CONFIG["KAFKA_SASL_PASSWORD"],
	})

	if err != nil {
		log.Printf("Failed to create producer: %s", err)
		return nil, err
	}

	log.Printf("Producer: %+v\n", producer)
	return producer, nil
}

func GetLastSchema(topic string) (*srclient.Schema, error) {
	topic = topic + "-value"

	schemaRegistryClient := srclient.CreateSchemaRegistryClient(CONFIG["KAFKA_SCHEMA_REGISTRY_URL"])
	schemaRegistryClient.SetCredentials(CONFIG["KAFKA_SASL_USERNAME"], CONFIG["KAFKA_SASL_PASSWORD"])

	schema, errSchema := schemaRegistryClient.GetLatestSchema(topic)
	if errSchema != nil {
		log.Panicln("Failed to get schema: ", errSchema.Error())
		return nil, errSchema
	}
	log.Printf("getting schema: %+v\n", schema)

	return schema, nil
}
