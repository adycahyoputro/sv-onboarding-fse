package configs

import (
	"log"
	"os"
	"time"
	"user-backend/models/entity"

	"github.com/google/uuid"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var (
	DB  *gorm.DB
	Err error
)

func ConnectMySQL() *gorm.DB {
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold:             time.Second, // Slow SQL threshold
			LogLevel:                  logger.Info, // Log level
			IgnoreRecordNotFoundError: true,        // Ignore ErrRecordNotFound error for logger
			Colorful:                  false,       // Disable color
		},
	)

	dsn := "root:" + CONFIG["MYSQL_PASS"] + "@tcp(" + CONFIG["MYSQL_HOST"] + ")/user_model?charset=utf8&parseTime=True"
	DB, Err := gorm.Open(mysql.Open(dsn), &gorm.Config{Logger: newLogger})

	if Err != nil {
		log.Println("Connection failed", Err)
	} else {
		log.Println("Server success")
	}

	sqlDB, errDB := DB.DB()
	if errDB != nil {
		log.Println(errDB)
	} else {
		sqlDB.SetMaxIdleConns(2)
		sqlDB.SetMaxOpenConns(1000)
	}

	DB.AutoMigrate(&entity.User{})
	DB.AutoMigrate(&entity.Role{})
	DB.AutoMigrate(&entity.Product{})

	role := entity.Role{}
	roleList := DB.Model(&entity.Role{}).Select("ID").Scan(&role)
	if roleList.RowsAffected == 0 {

		var role = [5]string{"admin", "maker", "checker", "signer", "viewer"}
		for i := 0; i < len(role); i++ {
			ID := uuid.New()
			DB.Create(&entity.Role{ID: ID, Title: role[i], Active: false})
		}
	}
	return DB
}
