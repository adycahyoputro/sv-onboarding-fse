package configs

import "os"

const (
	LOCAL       = "local"
	DEVELOPMENT = "development"
	DOCKERIZED  = "dokerized"
)

//Environment
const Environment string = DOCKERIZED

var env = map[string]map[string]string{
	"local": {
		//MYSQL
		"MYSQL_PORT":   "8080",
		"MYSQL_HOST":   "localhost",
		"MYSQL_USER":   "root",
		"MYSQL_PASS":   "",
		"MYSQL_SCHEMA": "user_model",

		"APP_NAME":   "user",
		"SECRET_KEY": "secret",

		//KAFKA
		"KAFKA_SERVERS":             "localhost/127.0.0.1:2181",
		"KAFKA_SCHEMA_REGISTRY_URL": "",
		"KAFKA_SASL_USERNAME":       "",
		"KAFKA_SASL_PASSWORD":       "",

		"PRODUCE_TO_KAFKA":              "true",
		"KAFKA_PRODUCED_TOPIC_FEEDBACK": "",
		"KAFKA_AVRO_SCHEMA_FEEDBACK":    "",
	},
	"development": {
		"MYSQL_PORT":   "8080",
		"MYSQL_HOST":   "localhost",
		"MYSQL_USER":   "root",
		"MYSQL_PASS":   "root",
		"MYSQL_SCHEMA": "user_model",
		"APP_NAME":     "user",
		"SECRET_KEY":   "secret",
	},
	"dokerized": {
		"MYSQL_PORT":   "8080",
		"MYSQL_HOST":   "localhost",
		"MYSQL_USER":   "root",
		"MYSQL_PASS":   "root",
		"MYSQL_SCHEMA": "user_model",
		"APP_NAME":     "user",
		"SECRET_KEY":   "secret",
	},
}

// CONFIG : global configuration
var CONFIG = env[Environment]

// Getenv : function for Environment Lookup
func Getenv(key string, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

func InitConfig() {
	for key := range CONFIG {
		CONFIG[key] = Getenv(key, CONFIG[key])
		os.Setenv(key, CONFIG[key])
	}
}
