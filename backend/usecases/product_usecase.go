package usecases

import (
	"errors"
	"user-backend/models/dto"
	"user-backend/models/entity"
	"user-backend/utils"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

func (product *productUseCase) GetAllProducts() dto.Response {
	productlist, err := product.productRepo.GetAllProducts()
	if err != nil && errors.Is(err, gorm.ErrRecordNotFound) {
		return utils.ResponseError("Data not found", err, 404)
	} else if err != nil {
		return utils.ResponseError("Internal server error", err, 500)
	}

	response := []dto.ProductList{}
	for _, product := range productlist {
		resProduct := dto.ProductList{
			ID:          product.ID,
			Name:        product.Name,
			Description: product.Description,
			Status:      product.Status,
		}
		response = append(response, resProduct)
	}

	return utils.ResponseSuccess("ok", nil, response, 200)
}
func (product *productUseCase) GetAllProductsChecker() dto.Response {
	productlist, err := product.productRepo.GetAllProductsChecker()
	if err != nil && errors.Is(err, gorm.ErrRecordNotFound) {
		return utils.ResponseError("Data not found", err, 404)
	} else if err != nil {
		return utils.ResponseError("Internal server error", err, 500)
	}

	response := []dto.ProductList{}
	for _, product := range productlist {
		resProduct := dto.ProductList{
			ID:          product.ID,
			Name:        product.Name,
			Description: product.Description,
			Status:      product.Status,
		}
		response = append(response, resProduct)
	}

	return utils.ResponseSuccess("ok", nil, response, 200)
}

func (product *productUseCase) GetAllProductsSigner() dto.Response {
	productlist, err := product.productRepo.GetAllProductsSigner()
	if err != nil && errors.Is(err, gorm.ErrRecordNotFound) {
		return utils.ResponseError("Data not found", err, 404)
	} else if err != nil {
		return utils.ResponseError("Internal server error", err, 500)
	}

	response := []dto.ProductList{}
	for _, product := range productlist {
		resProduct := dto.ProductList{
			ID:          product.ID,
			Name:        product.Name,
			Description: product.Description,
			Status:      product.Status,
		}
		response = append(response, resProduct)
	}

	return utils.ResponseSuccess("ok", nil, response, 200)
}

func (product *productUseCase) GetProductById(id string) dto.Response {
	productUUID, err := uuid.Parse(id)

	if err != nil {
		return utils.ResponseError("Data not found", err, 404)
	}
	productData, userData, err := product.productRepo.GetProductById(productUUID)

	if err != nil && errors.Is(err, gorm.ErrRecordNotFound) {
		return utils.ResponseError("Data not found", err, 404)
	} else if err != nil {
		return utils.ResponseError("Internal server error", err, 500)
	}

	maker := dto.Action{}
	checker := dto.Action{}
	signer := dto.Action{}
	for _, user := range userData {
		if user.ID == productData.MakerID {
			maker.ID = user.ID
			maker.Name = user.Name
		}
		if user.ID == productData.CheckerID {
			checker.ID = user.ID
			checker.Name = user.Name
		}
		if user.ID == productData.SignerID {
			signer.ID = user.ID
			signer.Name = user.Name
		}
	}
	response := dto.ProductDetail{
		ID:          productData.ID,
		Name:        productData.Name,
		Description: productData.Description,
		Status:      productData.Status,
		Maker:       maker,
		Checker:     checker,
		Signer:      signer,
	}

	return utils.ResponseSuccess("ok", nil, response, 200)
}

func (product *productUseCase) CreateNewProduct(newProduct dto.Product) dto.Response {
	userInsert := entity.Product{
		ID:          newProduct.ID,
		Name:        newProduct.Name,
		Description: newProduct.Description,
		Status:      "inactive",
		MakerID:     newProduct.MakerID,
	}

	userData, err := product.productRepo.CreateNewProduct(userInsert)

	if err != nil {
		if err.Error() == "Product has registered" {
			return utils.ResponseError("Product has registered", err, 400)
		}
		return utils.ResponseError("Internal server error", err, 500)
	}

	return utils.ResponseSuccess("ok", nil, map[string]interface{}{
		"id": userData.ID}, 201)
}

func (product *productUseCase) UpdateProductData(productUpdate dto.Product, id string) dto.Response {
	productInsert := entity.Product{
		Name:        productUpdate.Name,
		Description: productUpdate.Description,
	}
	_, err := product.productRepo.UpdateProductData(productInsert, id)

	if err != nil && errors.Is(err, gorm.ErrRecordNotFound) {
		return utils.ResponseError("Data not found", err, 404)
	} else if err != nil {
		return utils.ResponseError("Internal server error", err, 500)
	}
	return utils.ResponseSuccess("ok", nil, map[string]interface{}{"id": id}, 200)
}

func (product *productUseCase) UpdateCheckedProduct(productUpdate dto.Product, id string) dto.Response {
	productInsert := entity.Product{
		Name:        productUpdate.Name,
		Description: productUpdate.Description,
		Status:      "approved",
		CheckerID:   productUpdate.CheckerID,
	}
	_, err := product.productRepo.UpdateCheckedProduct(productInsert, id)

	if err != nil && errors.Is(err, gorm.ErrRecordNotFound) {
		return utils.ResponseError("Data not found", err, 404)
	} else if err != nil {
		return utils.ResponseError("Internal server error", err, 500)
	}
	return utils.ResponseSuccess("ok", nil, map[string]interface{}{"id": id}, 200)
}
func (product *productUseCase) UpdatePublishedProduct(productUpdate dto.Product, id string) dto.Response {
	productInsert := entity.Product{
		Name:        productUpdate.Name,
		Description: productUpdate.Description,
		Status:      "active",
		SignerID:    productUpdate.SignerID,
	}
	_, err := product.productRepo.UpdatePublishedProduct(productInsert, id)

	if err != nil && errors.Is(err, gorm.ErrRecordNotFound) {
		return utils.ResponseError("Data not found", err, 404)
	} else if err != nil {
		return utils.ResponseError("Internal server error", err, 500)
	}
	return utils.ResponseSuccess("ok", nil, map[string]interface{}{"id": id}, 200)
}

func (product *productUseCase) DeleteProductById(id string) dto.Response {

	err := product.productRepo.DeleteProductById(id)

	if err != nil && errors.Is(err, gorm.ErrRecordNotFound) {
		return utils.ResponseError("Data not found", err, 404)
	} else if err != nil {
		return utils.ResponseError("Internal server error", err, 500)
	}
	return utils.ResponseSuccess("ok", nil, nil, 200)
}
