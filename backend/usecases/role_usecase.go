package usecases

import (
	"user-backend/models/dto"
	"user-backend/utils"

	"gorm.io/gorm"
)

func (role *roleUseCase) RoleList() dto.Response {
	roleList, errRepo := role.roleRepo.RoleList()
	result := []dto.Role{}
	for _, role := range roleList {
		// userUUID, err := uuid.Parse(user.RoleId)
		role := dto.Role{ID: role.ID, Title: role.Title}
		result = append(result, role)
	}
	if errRepo != nil && (gorm.ErrRecordNotFound == errRepo) {
		return utils.ResponseError("Data not found", errRepo, 404)
	} else if errRepo != nil {
		return utils.ResponseError("Internal server error", errRepo, 500)
	}

	return utils.ResponseSuccess("ok", nil, result, 200)
}
