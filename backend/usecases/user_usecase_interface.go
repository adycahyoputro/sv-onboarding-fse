package usecases

import (
	"user-backend/models/dto"
	"user-backend/repositories"
)

type UserUseCase interface {
	UserList() dto.Response
	UserDetailById(string) dto.Response
	InsertNewUser(dto.User) dto.Response
	UpdateUserData(dto.User, string) dto.Response
	DeleteUserById(string) dto.Response
	UserLogin(dto.UserLogin) dto.Response
}

type userUseCase struct {
	userRepo repositories.UserRepository
	jwtAuth  JwtUseCase
}

func InsertUseCase(userRepository repositories.UserRepository, jwtAuthentication JwtUseCase) UserUseCase {
	return &userUseCase{
		userRepo: userRepository,
		jwtAuth:  jwtAuthentication,
	}
}
