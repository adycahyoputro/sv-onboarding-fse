package usecases

import (
	"user-backend/models/dto"
	"user-backend/repositories"
)

type RoleUseCase interface {
	RoleList() dto.Response
}

type roleUseCase struct {
	roleRepo repositories.RoleRepository
}

func GetRoleUseCase(roleRepository repositories.RoleRepository) RoleUseCase {
	return &roleUseCase{
		roleRepo: roleRepository,
	}
}
