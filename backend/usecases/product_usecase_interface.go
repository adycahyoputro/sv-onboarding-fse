package usecases

import (
	"user-backend/models/dto"
	"user-backend/repositories"
)

type ProductUseCase interface {
	GetAllProducts() dto.Response
	GetAllProductsChecker() dto.Response
	GetAllProductsSigner() dto.Response
	GetProductById(string) dto.Response
	CreateNewProduct(dto.Product) dto.Response
	UpdateProductData(dto.Product, string) dto.Response
	UpdateCheckedProduct(dto.Product, string) dto.Response
	UpdatePublishedProduct(dto.Product, string) dto.Response
	DeleteProductById(string) dto.Response
}

type productUseCase struct {
	productRepo repositories.ProductRepository
}

func GetProductUsecase(productRepository repositories.ProductRepository) ProductUseCase {
	return &productUseCase{
		productRepo: productRepository,
	}
}
