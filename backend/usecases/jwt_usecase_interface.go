package usecases

import (
	"user-backend/repositories"

	"github.com/golang-jwt/jwt"
)

type JwtUseCase interface {
	GenerateToken(string, string) (string, error)
	ValidateToken(string) (*jwt.Token, error)
	ValidateTokenAndGetUserId(string) (string, error)
	ValidateTokenAndGetRole(string) (string, error)
}

type jwtUseCase struct {
	userRepo repositories.UserRepository
}

func GetJwtUsecase(userRepository repositories.UserRepository) JwtUseCase {
	return &jwtUseCase{
		userRepo: userRepository,
	}
}
