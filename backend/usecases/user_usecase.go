package usecases

import (
	"user-backend/models/dto"
	"user-backend/models/entity"
	"user-backend/utils"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

func (user *userUseCase) UserList() dto.Response {
	userList, errRepo := user.userRepo.UserList()
	result := []dto.UserList{}
	for _, user := range userList {
		// userUUID, err := uuid.Parse(user.RoleId)
		role := dto.Role{ID: user.RoleId, Title: user.Title}
		resultData := dto.UserList{
			ID:     user.ID,
			Name:   user.Name,
			Role:   role,
			Active: user.Active,
		}
		result = append(result, resultData)
	}
	if errRepo != nil && (gorm.ErrRecordNotFound == errRepo) {
		return utils.ResponseError("Data not found", errRepo, 404)
	} else if errRepo != nil {
		return utils.ResponseError("Internal server error", errRepo, 500)
	}

	return utils.ResponseSuccess("ok", nil, result, 200)
}

func (user *userUseCase) UserDetailById(id string) dto.Response {
	userUUID, err := uuid.Parse(id)

	if err != nil {
		return utils.ResponseError("Data not found", err, 404)
	}
	userDetail, errRepo := user.userRepo.UserDetailById(userUUID)

	if errRepo != nil && (gorm.ErrRecordNotFound == errRepo) {
		return utils.ResponseError("Data not found", errRepo, 404)
	} else if errRepo != nil {
		return utils.ResponseError("Internal server error", errRepo, 500)
	}

	role := dto.Role{
		ID:    userDetail.RoleId,
		Title: userDetail.Title,
	}

	userResponse := dto.UserDetail{
		ID:             userDetail.ID,
		Name:           userDetail.Name,
		Email:          userDetail.Email,
		Role:           role,
		PersonalNumber: userDetail.PersonalNumber,
		Active:         userDetail.Active,
	}
	return utils.ResponseSuccess("ok", nil, userResponse, 200)
}

func (user *userUseCase) InsertNewUser(newUser dto.User) dto.Response {
	userInsert := entity.User{
		ID:             newUser.ID,
		Name:           newUser.Name,
		Email:          newUser.Email,
		PersonalNumber: newUser.PersonalNumber,
		Password:       newUser.Password,
	}

	userData, _, response := user.userRepo.InsertNewUser(userInsert)

	if response.Status != "ok" {
		if response.StatusCode == 403 {
			return utils.ResponseError("Email or Personal Number has registered", gorm.ErrRegistered, 403)
		} else if response.StatusCode == 404 {
			return utils.ResponseError("role not found", gorm.ErrRecordNotFound, 404)
		} else {
			return utils.ResponseError("Internal server error", gorm.ErrInvalidDB, 500)
		}
	}

	return utils.ResponseSuccess("ok", nil, map[string]interface{}{
		"id": userData.ID}, 201)
}

func (user *userUseCase) UpdateUserData(userUpdate dto.User, id string) dto.Response {
	if userUpdate.Password != "" {
		userUpdate.Password, _ = utils.HashPassword(userUpdate.Password)
	}
	userInsert := entity.User{
		Name:           userUpdate.Name,
		Email:          userUpdate.Email,
		PersonalNumber: userUpdate.PersonalNumber,
		Active:         userUpdate.Active,
		Password:       userUpdate.Password,
		RoleId:         userUpdate.Role.ID,
	}

	_, errRepo := user.userRepo.UpdateUserData(userInsert, id)

	if errRepo != nil && (gorm.ErrRecordNotFound == errRepo) {
		return utils.ResponseError("Data not found", errRepo, 404)
	} else if errRepo != nil {
		if errRepo.Error() == "Email or Personal Number has registered" {
			return utils.ResponseError("Email or Personal Number has registered", errRepo, 400)
		}
		return utils.ResponseError("Internal server error", errRepo, 500)
	}

	userUUID, err := uuid.Parse(id)
	if err != nil {
		return utils.ResponseError("Data not found", err, 404)
	}

	userUpdate.ID = userUUID
	return utils.ResponseSuccess("ok", nil, map[string]interface{}{"id": id}, 200)
}

func (user *userUseCase) DeleteUserById(id string) dto.Response {
	userUUID, err := uuid.Parse(id)
	if err != nil {
		return utils.ResponseError("Data not found", err, 404)
	}

	error := user.userRepo.DeleteUserById(userUUID)

	if error != nil && (gorm.ErrRecordNotFound == error) {
		return utils.ResponseError("Data not found", error, 404)
	} else if error != nil {
		return utils.ResponseError("Internal server error", error, 500)
	}
	return utils.ResponseSuccess("ok", nil, nil, 200)
}

func (user *userUseCase) UserLogin(userLogin dto.UserLogin) dto.Response {
	userData, err := user.userRepo.GetUserByPersonalNumber(userLogin.PersonalNumber)

	if err != nil {
		return utils.ResponseError("User not found", map[string]interface{}{"message": "Personal Number not found"}, 404)
	}

	errPwd := utils.CheckPasswordHash(userLogin.Password, userData.Password)

	if errPwd != nil {
		return utils.ResponseError("User not found", map[string]interface{}{"message": "Wrong Password"}, 404)
	}

	jwt, _ := user.jwtAuth.GenerateToken(userData.ID.String(), userData.RoleId.String())

	return utils.ResponseSuccess("ok", nil, map[string]interface{}{"token": jwt, "id": userData.ID}, 200)
}
