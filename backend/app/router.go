package app

import (
	"user-backend/configs"
	"user-backend/delivery"
	"user-backend/middleware"
	"user-backend/repositories"
	"user-backend/usecases"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	// "gorm.io/gorm"
)

func HandlerRequest() {
	configs.InitConfig()

	connection := configs.ConnectMySQL()

	userRepository := repositories.GetRepository(connection)
	jwtAuth := usecases.GetJwtUsecase(userRepository)
	userUseCase := usecases.InsertUseCase(userRepository, jwtAuth)
	userDelivery := delivery.InsertDelivery(userUseCase)

	productRepository := repositories.GetProductRepository(connection)
	productUseCase := usecases.GetProductUsecase(productRepository)
	productDelivery := delivery.GetProductDelivery(productUseCase)

	roleRepository := repositories.GetRoleRepository(connection)
	roleUseCase := usecases.GetRoleUseCase(roleRepository)
	roleDelivery := delivery.GetRoleDelivery(roleUseCase)

	router := gin.Default()

	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"*"},
		AllowHeaders:     []string{"*"},
		ExposeHeaders:    []string{"*"},
		AllowCredentials: true,
	}))

	protectedRoutes := router.Group("/")
	protectedRoutes.Use(middleware.JWTAuth(jwtAuth))
	{
		protectedRoutes.GET("/users", userDelivery.UserList)
		protectedRoutes.GET("/user/:id", userDelivery.UserDetailById)
		protectedRoutes.GET("/products", productDelivery.GetAllProducts)
		protectedRoutes.GET("/productsChecked", productDelivery.GetAllProductsChecker)
		protectedRoutes.GET("/productsSigned", productDelivery.GetAllProductsSigner)
		protectedRoutes.GET("/product/:id", productDelivery.GetProductById)
		protectedRoutes.POST("/product", productDelivery.CreateNewProduct)
		protectedRoutes.GET("/roles", roleDelivery.RoleList)
	}

	adminRoutes := router.Group("/")
	adminRoutes.Use(middleware.JWTAuthAdmin(jwtAuth, userRepository))
	{
		adminRoutes.PUT("/user/:id", userDelivery.UpdateUserData)
		adminRoutes.DELETE("/user/:id", userDelivery.DeleteUserById)
		adminRoutes.PUT("/product/:id", productDelivery.UpdateProductData)
		adminRoutes.DELETE("/product/:id", productDelivery.DeleteProductById)

	}

	checkerRoutes := router.Group("/")
	checkerRoutes.Use(middleware.JWTAuthChecker(jwtAuth, userRepository))
	{
		checkerRoutes.PUT("/product/:id/checked", productDelivery.UpdateCheckedProduct)
	}

	signerRoutes := router.Group("/")
	signerRoutes.Use(middleware.JWTAuthSigner(jwtAuth, userRepository))
	{
		signerRoutes.PUT("/product/:id/signer", productDelivery.UpdatePublishedProduct)
	}

	router.POST("/user", userDelivery.InsertNewUser)
	router.POST("/login", userDelivery.UserLogin)

	router.Run(":8080")
}
