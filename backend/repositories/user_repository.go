package repositories

import (
	"errors"
	"user-backend/models/dto"
	"user-backend/models/entity"
	"user-backend/utils"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

func (userRepo *userRepository) UserList() ([]entity.UserList, error) {
	// userlist := []entity.UserList{}
	var userList []entity.UserList
	err := userRepo.DB.Model(&entity.User{}).Select("users.name, users.active, users.id, roles.title, users.roleId").Joins("left join roles on roles.id = users.roleId").Scan(&userList).Error
	//check error execute query
	if err != nil {
		return nil, err
	}
	//check userlist exits
	if len(userList) == 0 {
		return nil, gorm.ErrRecordNotFound
	}
	return userList, nil
}

func (userRepo *userRepository) UserDetailById(id uuid.UUID) (*entity.UserDetail, error) {
	userdetail := entity.UserDetail{}
	err := userRepo.DB.Model(&entity.User{}).Where("users.id = ?", id).Select("users.name, users.active, users.email, users.personalNumber, users.id, roles.title, users.roleId").Joins("left join roles on roles.id = users.roleId").First(&userdetail).Error
	//check user exist or not
	if err != nil {
		return nil, gorm.ErrRecordNotFound
	}

	// if (userdetail == entity.UserDetail{}) {
	// 	return nil, gorm.ErrRecordNotFound
	// }

	return &userdetail, nil
}

func (userRepo *userRepository) InsertNewUser(user entity.User) (*entity.User, *entity.Role, dto.Response) {
	role := entity.Role{}

	result := userRepo.DB.Where("email = ? OR personalNumber = ?", user.Email, user.PersonalNumber).Find(&user)
	//check user already exits or not
	if result.RowsAffected > 0 {
		resp := utils.ResponseError("Email or Personal Number has registered", gorm.ErrRegistered, 403)
		return nil, nil, resp
	}

	user.ID = uuid.New()
	hash, _ := utils.HashPassword(user.Password)
	user.Password = hash
	//check user role
	if err := userRepo.DB.Where("title = ?", "viewer").Find(&role).Error; err != nil {
		resp := utils.ResponseError("role not found", gorm.ErrRecordNotFound, 404)
		return nil, nil, resp
	}

	user.RoleId = role.ID
	//check proses insert user
	if err := userRepo.DB.Create(&user).Error; err != nil {
		resp := utils.ResponseError("create new usser failed", gorm.ErrInvalidDB, 500)
		return nil, nil, resp
	}

	return &user, &role, utils.ResponseSuccess("ok", nil, map[string]interface{}{
		"id": user.ID}, 201)
}

func (userRepo *userRepository) UpdateUserData(user entity.User, id string) (*entity.User, error) {
	// role := entity.Role{}

	resultEmail := userRepo.DB.Where("email = ? OR personalNumber = ?", user.Email, user.PersonalNumber).Find(&user)
	//check user already exits or not
	if resultEmail.RowsAffected > 0 {
		return nil, errors.New("Email or Personal Number has registered")
	}

	// if user.Password != "" {
	// 	user.Password, _ = utils.HashPassword(user.Password)
	// } else {

	// }
	result := userRepo.DB.Model(&user).Where("id = ?", id).Updates(&user)

	if result.RowsAffected == 0 {
		return nil, gorm.ErrRecordNotFound
	}

	return &user, nil
}

func (userRepo *userRepository) DeleteUserById(id uuid.UUID) error {
	// sql := "DELETE FROM users"
	// sql = fmt.Sprintf("%s WHERE id = '%s'", sql, id)
	// if err := userRepo.DB.Raw(sql).Scan(entity.User{}).Error; err != nil {
	// 	return err
	// }
	user := entity.User{}
	result := userRepo.DB.Where("id = ?", id).Find(&user)
	if result.RowsAffected == 0 {
		return gorm.ErrRecordNotFound
	}
	if err := userRepo.DB.Delete(&entity.User{}, id).Error; err != nil {
		return err
	}
	return nil
}

func (userRepo *userRepository) GetRoleByRoleId(id uuid.UUID) (*entity.Role, error) {
	role := entity.Role{}
	result := userRepo.DB.Where("id = ?", id).Find(&role)
	if result.RowsAffected == 0 {
		return nil, gorm.ErrRecordNotFound
	}

	return &role, nil
}

func (userRepo *userRepository) GetUserByPersonalNumber(pn string) (*entity.User, error) {
	user := entity.User{}
	result := userRepo.DB.Where("personalNumber = ?", pn).Find(&user)
	if result.RowsAffected == 0 {
		return nil, gorm.ErrRecordNotFound
	}

	return &user, nil
}
