package repositories

import (
	"user-backend/models/dto"
	"user-backend/models/entity"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type UserRepository interface {
	UserList() ([]entity.UserList, error)
	UserDetailById(uuid.UUID) (*entity.UserDetail, error)
	InsertNewUser(entity.User) (*entity.User, *entity.Role, dto.Response)
	UpdateUserData(entity.User, string) (*entity.User, error)
	DeleteUserById(uuid.UUID) error
	GetRoleByRoleId(uuid.UUID) (*entity.Role, error)
	GetUserByPersonalNumber(string) (*entity.User, error)
}

type userRepository struct {
	DB *gorm.DB
}

func GetRepository(DB *gorm.DB) UserRepository {
	return &userRepository{
		DB: DB,
	}
}
