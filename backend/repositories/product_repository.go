package repositories

import (
	"errors"
	"user-backend/models/entity"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

func (productRepo *productRepository) GetAllProducts() ([]entity.Product, error) {
	products := []entity.Product{}
	err := productRepo.DB.Model(&entity.Product{}).Where("status = 'inactive'").Scan(&products).Error
	if err != nil {
		return nil, err
	}

	if len(products) <= 0 {
		return nil, gorm.ErrRecordNotFound
	}

	return products, nil
}

func (productRepo *productRepository) GetAllProductsChecker() ([]entity.Product, error) {
	products := []entity.Product{}
	err := productRepo.DB.Model(&entity.Product{}).Where("status = 'approved'").Scan(&products).Error
	if err != nil {
		return nil, err
	}

	if len(products) <= 0 {
		return nil, gorm.ErrRecordNotFound
	}

	return products, nil
}

func (productRepo *productRepository) GetAllProductsSigner() ([]entity.Product, error) {
	products := []entity.Product{}
	err := productRepo.DB.Model(&entity.Product{}).Where("status = 'active'").Scan(&products).Error
	if err != nil {
		return nil, err
	}

	if len(products) <= 0 {
		return nil, gorm.ErrRecordNotFound
	}

	return products, nil
}

func (productRepo *productRepository) GetProductById(id uuid.UUID) (*entity.Product, []entity.User, error) {
	// productDetail := entity.ProductDetail{}
	product := entity.Product{}
	users := []entity.User{}

	// if err := productRepo.DB.Model(&product).Where("products.id = ?", id).Select("products.id, products.name, products.description, products.status,  products.makerId, products.signerId, products.checkerId").Scan(&productDetail).Error; err != nil {
	if err := productRepo.DB.Where("id = ?", id).Find(&product).Error; err != nil {
		return nil, nil, err
	}

	// fmt.Println(product)
	if (entity.Product{}) == product {
		return nil, nil, gorm.ErrRecordNotFound
	}

	err := productRepo.DB.Where("id IN ?", []uuid.UUID{product.MakerID, product.CheckerID, product.SignerID}).Find(&users).Error
	if err != nil {
		return nil, nil, err
	}

	// fmt.Println(users)

	return &product, users, nil
}

func (productRepo *productRepository) CreateNewProduct(product entity.Product) (*entity.Product, error) {
	result := productRepo.DB.Where("name = ?", product.Name).Find(&product)
	//check product already exits or not
	if result.RowsAffected > 0 {
		return nil, errors.New("Product has registered")
	}
	product.ID = uuid.New()

	if err := productRepo.DB.Create(&product).Error; err != nil {
		return nil, err
	}
	return &product, nil
}

func (productRepo *productRepository) UpdateProductData(product entity.Product, id string) (*entity.Product, error) {
	result := productRepo.DB.Model(&product).Where("id = ?", id).Updates(&product)
	if result.RowsAffected == 0 {
		return nil, gorm.ErrRecordNotFound
	}

	return &product, nil
}

func (productRepo *productRepository) UpdateCheckedProduct(product entity.Product, id string) (*entity.Product, error) {
	result := productRepo.DB.Model(&product).Where("id = ?", id).Updates(&product)
	if result.RowsAffected == 0 {
		return nil, gorm.ErrRecordNotFound
	}
	return &product, nil
}

func (productRepo *productRepository) UpdatePublishedProduct(product entity.Product, id string) (*entity.Product, error) {
	result := productRepo.DB.Model(&product).Where("id = ?", id).Updates(&product)
	if result.RowsAffected == 0 {
		return nil, gorm.ErrRecordNotFound
	}
	return &product, nil
}

func (productRepo *productRepository) DeleteProductById(id string) error {
	product := entity.Product{}
	result := productRepo.DB.Where("id = ?", id).Find(&product)
	if result.RowsAffected == 0 {
		return gorm.ErrRecordNotFound
	}
	if err := productRepo.DB.Delete(&entity.Product{}, id).Error; err != nil {
		return err
	}
	return nil
}
