package repositories

import (
	"user-backend/models/entity"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type ProductRepository interface {
	GetAllProducts() ([]entity.Product, error)
	GetAllProductsChecker() ([]entity.Product, error)
	GetAllProductsSigner() ([]entity.Product, error)
	GetProductById(uuid.UUID) (*entity.Product, []entity.User, error)
	CreateNewProduct(entity.Product) (*entity.Product, error)
	UpdateProductData(entity.Product, string) (*entity.Product, error)
	UpdateCheckedProduct(entity.Product, string) (*entity.Product, error)
	UpdatePublishedProduct(entity.Product, string) (*entity.Product, error)
	DeleteProductById(string) error
}

type productRepository struct {
	DB *gorm.DB
}

func GetProductRepository(DB *gorm.DB) ProductRepository {
	return &productRepository{
		DB: DB,
	}
}
