package repositories

import (
	"user-backend/models/entity"

	"gorm.io/gorm"
)

type RoleRepository interface {
	RoleList() ([]entity.Role, error)
}

type roleRepository struct {
	DB *gorm.DB
}

func GetRoleRepository(DB *gorm.DB) RoleRepository {
	return &roleRepository{
		DB: DB,
	}
}
