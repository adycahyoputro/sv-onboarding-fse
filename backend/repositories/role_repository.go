package repositories

import (
	"user-backend/models/entity"

	"gorm.io/gorm"
)

func (roleRepo *roleRepository) RoleList() ([]entity.Role, error) {
	var roleList []entity.Role
	err := roleRepo.DB.Model(&entity.Role{}).Select("id, title").Scan(&roleList).Error
	//check error execute query
	if err != nil {
		return nil, err
	}
	//check userlist exits
	if len(roleList) == 0 {
		return nil, gorm.ErrRecordNotFound
	}
	return roleList, nil
}
