package repositories

import (
	"user-backend/models/entity"

	"github.com/stretchr/testify/mock"
	"gorm.io/gorm"
)

type UserRepositoryMockInterface interface {
}
type UserRepositoryMock struct {
	Mock mock.Mock
}

func (userRepo *UserRepositoryMock) GetUsers() ([]entity.UserList, error) {
	arguments := userRepo.Mock.Called()

	if arguments.Get(1) != nil {
		var err = arguments.Get(1).(error)
		// err := arguments.Get(1).(error)
		return nil, err
	}

	if arguments.Get(0) == nil && arguments.Get(1) == nil {
		return nil, gorm.ErrRecordNotFound
	}

	userlist := arguments.Get(0).([]entity.UserList)
	return userlist, nil
}
