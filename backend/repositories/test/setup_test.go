package test

import (
	"database/sql"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
	"user-backend/repositories"

	"github.com/DATA-DOG/go-sqlmock"
	// "github.com/Shopify/sarama/mocks"
	"github.com/gin-gonic/gin"
	"go.elastic.co/apm"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var repo repositories.UserRepository
var mockSql sqlmock.Sqlmock
var db *sql.DB
var querySelect = "^SELECT (.+)"

func CreateUserTransaction() *apm.Transaction {
	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request = &http.Request{
		URL:    &url.URL{},
		Header: make(http.Header),
	}
	tx := apm.TransactionFromContext(c)
	return tx
}

func TestMain(m *testing.M) {
	gin.SetMode(gin.TestMode)
	var err error
	db, mockSql, err = sqlmock.New()
	if err != nil {
		panic("error sql mock")
	}
	defer db.Close()

	dsn := "root:@/user_model?charset=utf8&parseTime=True"
	dbMy, errMy := gorm.Open(mysql.Open(dsn))
	if errMy != nil {
		panic("error gorm")
	}

	repo = repositories.GetRepository(dbMy)
	m.Run()
}

// func initMySQL(t *testing.T) (sqlmock.Sqlmock, *gorm.DB) {
// 	mockedProducer := mocks.NewSyncProducer(t, nil)
// 	mockedProducer.ExpectSendMessageAndSucceed()

// 	db, sqlMock := NewSQLMock()
// 	mysql, _ := gorm.Open("mysql", db)

// 	return sqlMock, mysql
// }

// func NewSQLMock() {
// 	panic("unimplemented")
// }
