package test

import (
	"testing"
	"user-backend/models/entity"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestGetUserListSuccess(t *testing.T) {
	// tx := CreateUserTransaction()
	ID, _ := uuid.Parse("6a53df9b-198d-4126-a157-8518f8444507")
	roleID, _ := uuid.Parse("5d52046c-3e3a-430c-a04b-2ca39ab1a393")
	data1 := entity.UserList{
		ID:     ID,
		Name:   "admin",
		RoleId: roleID,
		Title:  "admin",
		Active: false,
	}
	rows := sqlmock.NewRows([]string{"id", "name", "roleId", "title", "active"}).
		AddRow(data1.ID, data1.Name, data1.RoleId, data1.Title, data1.Active)

	mockSql.ExpectQuery(querySelect).WillReturnRows(rows)

	userlist, errdb := repo.UserList()
	assert.Nil(t, errdb, "error nil")
	assert.NotNil(t, userlist, "data not nil")
}
