package delivery

import (
	"user-backend/usecases"

	"github.com/gin-gonic/gin"
)

type ProductDelivery interface {
	GetAllProducts(*gin.Context)
	GetAllProductsChecker(*gin.Context)
	GetAllProductsSigner(*gin.Context)
	GetProductById(*gin.Context)
	CreateNewProduct(*gin.Context)
	UpdateProductData(*gin.Context)
	UpdateCheckedProduct(*gin.Context)
	UpdatePublishedProduct(*gin.Context)
	DeleteProductById(*gin.Context)
}

type productDelivery struct {
	productUsecase usecases.ProductUseCase
}

func GetProductDelivery(usecase usecases.ProductUseCase) ProductDelivery {
	return &productDelivery{
		productUsecase: usecase,
	}
}
