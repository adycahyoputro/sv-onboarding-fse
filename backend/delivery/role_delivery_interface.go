package delivery

import (
	"user-backend/usecases"

	"github.com/gin-gonic/gin"
)

type RoleDelivery interface {
	RoleList(*gin.Context)
}

type roleDelivery struct {
	roleUseCase usecases.RoleUseCase
}

func GetRoleDelivery(roleUseCase usecases.RoleUseCase) RoleDelivery {
	return &roleDelivery{
		roleUseCase: roleUseCase,
	}
}
