package delivery

import (
	"net/http"
	"user-backend/models/dto"
	"user-backend/utils"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

func (product *productDelivery) GetAllProducts(c *gin.Context) {
	response := product.productUsecase.GetAllProducts()
	// fmt.Printf("%+v", response)
	if response.Status != "ok" {
		c.JSON(response.StatusCode, response)
		return
	}
	c.JSON(response.StatusCode, response)
}

func (product *productDelivery) GetAllProductsChecker(c *gin.Context) {
	response := product.productUsecase.GetAllProductsChecker()
	// fmt.Printf("%+v", response)
	if response.Status != "ok" {
		c.JSON(response.StatusCode, response)
		return
	}
	c.JSON(response.StatusCode, response)
}

func (product *productDelivery) GetAllProductsSigner(c *gin.Context) {
	response := product.productUsecase.GetAllProductsSigner()
	// fmt.Printf("%+v", response)
	if response.Status != "ok" {
		c.JSON(response.StatusCode, response)
		return
	}
	c.JSON(response.StatusCode, response)
}

func (product *productDelivery) GetProductById(c *gin.Context) {
	id := c.Param("id")
	response := product.productUsecase.GetProductById(id)

	if response.StatusCode == http.StatusNotFound {
		c.JSON(http.StatusOK, response)
		return
	}
	if response.Status != "ok" {
		c.JSON(response.StatusCode, response)
		return
	}
	c.JSON(http.StatusOK, response)
}

func (product *productDelivery) CreateNewProduct(c *gin.Context) {
	userId, _ := c.Get("user_id")
	userMakerId, _ := userId.(string)
	userMakerUUID, err := uuid.Parse(userMakerId)

	if err != nil {
		errorRes := utils.ResponseError("Data not found", err, 404)
		c.JSON(errorRes.StatusCode, errorRes)
		return
	}
	request := dto.Product{
		MakerID: userMakerUUID,
	}

	if err := c.ShouldBindJSON(&request); err != nil {

		errorRes := utils.ResponseError("Invalid Input", err, 400)
		c.JSON(errorRes.StatusCode, errorRes)
		return

	}
	response := product.productUsecase.CreateNewProduct(request)

	if response.Status != "ok" {
		c.JSON(response.StatusCode, response)
		return
	}

	c.JSON(response.StatusCode, response)
}

func (product *productDelivery) UpdateProductData(c *gin.Context) {
	id := c.Param("id")
	request := dto.Product{}

	if err := c.ShouldBindJSON(&request); err != nil {
		errorRes := utils.ResponseError("Invalid Input", err, 400)
		c.JSON(errorRes.StatusCode, errorRes)
		return
	}

	response := product.productUsecase.UpdateProductData(request, id)
	if response.Status != "ok" {
		c.JSON(response.StatusCode, response)
		return
	}

	c.JSON(response.StatusCode, response)
}

func (product *productDelivery) UpdateCheckedProduct(c *gin.Context) {
	id := c.Param("id")
	userId, _ := c.Get("user_id")
	userCheckedId, _ := userId.(string)
	userCheckedUUID, err := uuid.Parse(userCheckedId)

	if err != nil {
		errorRes := utils.ResponseError("Data not found", err, 404)
		c.JSON(errorRes.StatusCode, errorRes)
		return
	}
	request := dto.Product{
		CheckerID: userCheckedUUID,
	}

	if err := c.ShouldBindJSON(&request); err != nil {
		errorRes := utils.ResponseError("Invalid Input", err, 400)
		c.JSON(errorRes.StatusCode, errorRes)
		return
	}

	response := product.productUsecase.UpdateCheckedProduct(request, id)
	if response.Status != "ok" {
		c.JSON(response.StatusCode, response)
		return
	}

	c.JSON(response.StatusCode, response)
}
func (product *productDelivery) UpdatePublishedProduct(c *gin.Context) {
	id := c.Param("id")
	userId, _ := c.Get("user_id")
	userSignedId, _ := userId.(string)
	userSignedUUID, err := uuid.Parse(userSignedId)

	if err != nil {
		errorRes := utils.ResponseError("Data not found", err, 404)
		c.JSON(errorRes.StatusCode, errorRes)
		return
	}
	request := dto.Product{
		SignerID: userSignedUUID,
	}

	if err := c.ShouldBindJSON(&request); err != nil {
		errorRes := utils.ResponseError("Invalid Input", err, 400)
		c.JSON(errorRes.StatusCode, errorRes)
		return
	}

	response := product.productUsecase.UpdatePublishedProduct(request, id)
	if response.Status != "ok" {
		c.JSON(response.StatusCode, response)
		return
	}

	c.JSON(response.StatusCode, response)
}

func (product *productDelivery) DeleteProductById(c *gin.Context) {
	id := c.Param("id")
	response := product.productUsecase.DeleteProductById(id)
	if response.Status != "ok" {
		c.JSON(response.StatusCode, response)
		return
	}
	c.JSON(response.StatusCode, response)
}
