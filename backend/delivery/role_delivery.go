package delivery

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func (role *roleDelivery) RoleList(c *gin.Context) {
	response := role.roleUseCase.RoleList()
	if response.Status != "ok" {
		c.JSON(response.StatusCode, response)
		return
	}
	c.JSON(http.StatusOK, response)
}
