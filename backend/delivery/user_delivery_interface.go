package delivery

import (
	"user-backend/usecases"

	"github.com/gin-gonic/gin"
)

type UserDelivery interface {
	UserList(*gin.Context)
	UserDetailById(*gin.Context)
	InsertNewUser(*gin.Context)
	UpdateUserData(*gin.Context)
	DeleteUserById(*gin.Context)
	UserLogin(*gin.Context)
}

type userDelivery struct {
	userUseCase usecases.UserUseCase
}

func InsertDelivery(userUseCase usecases.UserUseCase) UserDelivery {
	return &userDelivery{
		userUseCase: userUseCase,
	}
}
