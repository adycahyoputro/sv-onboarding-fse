package entity

type Result struct {
	Status     string      `json:"status"`
	StatusCode int         `json:"statusCode"`
	Error      string      `json:"error"`
	Data       interface{} `json:"data"`
}
