package entity

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	ID             uuid.UUID `json:"id" gorm:"column:id;primaryKey"`
	PersonalNumber string    `json:"personalNumber" gorm:"column:personalNumber"`
	Password       string    `json:"password" gorm:"column:password"`
	Email          string    `json:"email" gorm:"column:email"`
	Name           string    `json:"name" gorm:"column:name"`
	RoleId         uuid.UUID `json:"roleId" gorm:"column:roleId"`
	// Role           Role      `json:"role" gorm:"foreignKey:RoleId"`
	Active bool `json:"active" gorm:"column:active"`
}

type UserList struct {
	ID     uuid.UUID `json:"id" gorm:"column:id;primaryKey"`
	Name   string    `json:"name" gorm:"column:name"`
	RoleId uuid.UUID `json:"roleId" gorm:"column:roleId"`
	Title  string    `json:"title" gorm:"column:title"`
	// Role   Role      `json:"role" gorm:"foreignKey:RoleId"`
	Active bool `json:"active" gorm:"column:active"`
}

type UserDetail struct {
	ID             uuid.UUID `json:"id" gorm:"column:id;primaryKey"`
	PersonalNumber string    `json:"personalNumber" gorm:"column:personalNumber"`
	Email          string    `json:"email" gorm:"column:email"`
	Name           string    `json:"name" gorm:"column:name"`
	Password       string    `json:"password" gorm:"column:password"`
	RoleId         uuid.UUID `json:"roleId" gorm:"column:roleId"`
	Title          string    `json:"title" gorm:"column:title"`
	// Role           Role      `json:"role" gorm:"foreignKey:RoleId"`
	Active bool `json:"active" gorm:"column:active"`
}
