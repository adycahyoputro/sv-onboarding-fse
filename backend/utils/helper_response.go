package utils

import "user-backend/models/dto"

func ResponseError(status string, err interface{}, code int) dto.Response {
	return dto.Response{
		Status:     status,
		StatusCode: code,
		Error:      err,
		Data:       nil,
	}
}

func ResponseSuccess(status string, err interface{}, data interface{}, code int) dto.Response {
	return dto.Response{
		Status:     status,
		StatusCode: code,
		Error:      err,
		Data:       data,
	}
}
